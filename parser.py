import logging

from tqdm import tqdm
from time import sleep
import selenium.common
import selenium.webdriver


def parse_search_results(
        driver: selenium.webdriver.Firefox,
        time_delta: float = 1.,
        desc: str = ""):

    search_results_list = []

    search_results = driver.find_element_by_id("search-results")
    results = search_results.find_elements_by_tag_name("li")

    for i in tqdm(range(len(results)), desc=desc):
        try:
            search_results = driver.find_element_by_id("search-results")
            results = search_results.find_elements_by_tag_name("li")
            results[i].find_element_by_tag_name("a").click()

            author_list_elem = driver.find_element_by_class_name("author-list")
            author_li_elems = author_list_elem.find_elements_by_tag_name("li")

            search_results_list.append({
                "title": driver.title,
                "authors": ", ".join([elem.text for elem in author_li_elems]),
                "annotation": driver.find_element_by_tag_name('p').text,
            })
        except selenium.common.exceptions.NoSuchElementException as e:
            logging.info(f"Exception: {e}")

        driver.back()
        sleep(time_delta)
    return search_results_list


def parse_query(
        driver: selenium.webdriver.Firefox,
        query: str,
        num_pages: int = 10):
    if not 1 <= num_pages <= 10:
        raise ValueError("Maximal number of pages to parse is 10.")

    driver.get("https://cyberleninka.ru/")
    search_form = driver.find_element_by_name('q')
    search_form.send_keys(query)
    search_form.submit()
    sleep(1.)

    search_results = []

    paginator = driver.find_element_by_class_name("paginator")
    pages = paginator.find_elements_by_tag_name("li")

    for i in range(1, len(pages)):
        search_results_i = parse_search_results(driver, time_delta=1., desc=f"Page {i:2d}")
        search_results.extend(search_results_i)

        paginator = driver.find_element_by_class_name("paginator")
        pages = paginator.find_elements_by_tag_name("li")

        if i == len(pages):
            return search_results
        pages[i].find_element_by_tag_name("a").click()
        sleep(2.)
    if num_pages == 1:
        i = 0
    search_results_i = parse_search_results(driver, time_delta=1., desc=f"Page {i + 1:2d}")
    search_results.extend(search_results_i)

    return search_results
