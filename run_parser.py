import os
import yaml
import codecs
import logging
import argparse

from sqlalchemy import insert
from sqlalchemy import create_engine
from sqlalchemy import MetaData
from sqlalchemy import Table, Column
from sqlalchemy import Integer, String

from selenium.webdriver import Firefox
from selenium.webdriver.firefox.options import Options

import parser


def main(args):
    with codecs.open(args.queries_config_path, "r") as f:
        config = yaml.load(f)
    topics = config["topics"]
    db_name = config["db_name"]

    if config["verbose"]:
        logging.basicConfig(format='%(asctime)s - %(name)s - %(levelname)s - %(message)s', level=logging.INFO)

    os.makedirs("data", exist_ok=True)
    engine = create_engine(f'sqlite:///data/{db_name}', echo=False)
    logging.info("Database engine was initialized.")

    tables = {}
    meta = MetaData()
    for name in topics:
        tables[name] = Table(
            name, meta,
            Column('id', Integer, primary_key=True),
            Column('title', String),
            Column('authors', String),
            Column('annotation', String),
        )
        logging.info(f"Table '{name}' was added to DB scheme.")
    meta.create_all(engine)
    logging.info(f"{len(tables)} tables were created.")

    opts = Options()
    opts.headless = True
    driver = Firefox(options=opts)
    logging.info("Firefox driver was initialized.")

    for name in topics:
        logging.info(f"Topic to parse: '{name}'.")
        queries = topics[name]
        for query in queries:
            logging.info(f"Query: '{query}'")
            results = parser.parse_query(
                driver, query, num_pages=config["num_pages_per_query"])
            engine.execute(insert(tables[name]), results)
    logging.info("Parsing is finished.")
    driver.close()
    logging.info("Firefox driver was closed.")


if __name__ == "__main__":
    args_parser = argparse.ArgumentParser()
    args_parser.add_argument("-c", "--queries_config_path", type=str,
                             help="Specifies the path to .yml file with queries to parse.")
    main(args=args_parser.parse_args())
